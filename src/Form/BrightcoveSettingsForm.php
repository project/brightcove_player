<?php

namespace Drupal\brightcove_player\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class BrightcoveSettingsForm extends ConfigFormBase {

    /**
     * Config settings.
     *
     * @var string
     */
    const SETTINGS = 'brightcove_player.settings';

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'brightcove_player_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            static::SETTINGS,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config(static::SETTINGS);

        $form['account_id'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Account ID'),
            '#default_value' => $config->get('account_id'),
        ];

        $form['policy_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Policy Key'),
            '#default_value' => $config->get('policy_key'),
            '#maxlength' => 250,
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $this->configFactory->getEditable(static::SETTINGS)
            // Set the submitted configuration setting.
            ->set('account_id', $form_state->getValue('account_id'))
            ->set('policy_key', $form_state->getValue('policy_key'))
            ->save();

        parent::submitForm($form, $form_state);
    }

}
