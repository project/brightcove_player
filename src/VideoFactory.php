<?php

declare(strict_types=1);

namespace Drupal\brightcove_player;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * Factory for loading videos from Brightcove.
 */
class VideoFactory {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $client;

  /**
   * The account ID to load videos from.
   *
   * @var string
   */
  private $accountId;

    /**
     * The player policy key.
     *
     * Policy keys are public and used in video requests by the player itself.
     * As well, they can never be revoked or changed.
     *
     * @see https://apis.support.brightcove.com/policy/getting-started/overview-policy-api.html
     *
     * @var string.
     */
  private $policyKey;

  /**
   * VideoFactory constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ClientInterface $client) {
    $this->client = $client;
    $config = $configFactory->get('brightcove_player.settings');
    $this->accountId = $config->get('account_id');
    $this->policyKey = $config->get('policy_key');
  }

  /**
   * Load a video from Brightcove.
   *
   * @param string $video_id
   *   The video ID to load.
   *
   * @return \Drupal\brightcove_player\Video
   *   The loaded video.
   */
  public function load(string $video_id): Video {
    $response = $this->client->get(
      "https://edge.api.brightcove.com/playback/v1/accounts/{$this->accountId}/videos/{$video_id}",
      [
        'headers' => [
          'Accept' => 'application/json;pk=' . $this->policyKey,
        ],
      ]
    );

    $data = \GuzzleHttp\json_decode($response->getBody(), TRUE);
    return Video::fromJson($data);
  }

  /**
   * Reload a loaded video.
   *
   * @param \Drupal\brightcove_player\Video $video
   *   The video object to reload.
   *
   * @return \Drupal\brightcove_player\Video
   *   The newly loaded video object.
   */
  public function loadFromVideo(Video $video): Video {
    return $this->load($video->getId());
  }

  /**
   * Load a video from a Studio URL.
   *
   * @param string $url
   *   The studio URL to parse the video ID to load from.
   *
   * @return \Drupal\brightcove_player\Video
   *   The loaded video.
   */
  public function loadFromStudioUrl(string $url): Video {
    $video = Video::fromStudioUrl($url);
    return $this->loadFromVideo($video);
  }

}
