<?php

declare(strict_types=1);

namespace Drupal\brightcove_player;

/**
 * Class representing video data from Brightcove.
 *
 * Note that more data may exist in the upstream Brightcove service, such as
 * when this class is constructed with an ID directly instead of calling
 * fromJson(). In that case, use \Drupal\brightcove_player\VideoFactory to
 * load the full data (at the cost of a remote HTTP call).
 */
class Video {

  /**
   * The player to use for videos.
   */
  const PLAYER_ID = 'default';

  /**
   * The account ID for Brightcove.
   *
   * @var string
   */
  private $accountId;

  /**
   * The ID of the video.
   *
   * @var string
   */
  private $videoId;

  /**
   * The name of the video.
   *
   * @var string
   */
  private $name = '';

  /**
   * The URL of the poster of the video.
   *
   * @var string
   */
  private $posterSource = '';

  /**
   * Video constructor.
   *
   * @param string $video_id
   *   The ID of this video.
   */
  public function __construct(string $video_id) {
    $this->videoId = $video_id;
    $this->accountId = \Drupal::config('brightcove_player.settings')->get('account_id');
  }

  /**
   * Construct a video from a BrightCove API response.
   *
   * @param array $json
   *   An array of JSON data.
   *
   * @return static
   *   A new video.
   *
   * @see \Drupal\brightcove_player\VideoFactory
   */
  public static function fromJson(array $json): self {
    $video = new Video($json['id']);
    $video->setName($json['name']);

    if (isset($json['poster_sources']['0']['src'])) {
      $video->setPosterSource($json['poster_sources']['0']['src']);
    }

    return $video;
  }

  /**
   * Construct a video from a studio URL.
   *
   * @param string $url
   *   The studio URL.
   *
   * @return static
   *   A new video.
   */
  public static function fromStudioUrl(string $url): self {
    $video_id = str_replace(
      'https://studio.brightcove.com/products/videocloud/media/videos/',
      '',
      $url
    );
    return new self($video_id);
  }

  /**
   * Retrieve the embed code source for a given video ID.
   *
   * @return string
   *   The source to use in an iframe.
   */
  public function getEmbedSrc() {
    $player_id = static::PLAYER_ID;
    return "https://players.brightcove.net/{$this->accountId}/{$player_id}_default/index.html?videoId={$this->videoId}";
  }

  /**
   * Get the name of the video.
   *
   * @return string
   *   The name of the video.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Set the name of the video.
   *
   * @param string $name
   *   The name to set.
   */
  public function setName(string $name): void {
    $this->name = $name;
  }

  /**
   * Get the poster source URL.
   *
   * @return string
   *   The poster source URL.
   */
  public function getPosterSource(): string {
    return $this->posterSource;
  }

  /**
   * Set the source URL of the poster.
   *
   * @param string $poster
   *   The URL to set.
   */
  public function setPosterSource(string $poster): void {
    $this->posterSource = $poster;
  }

  /**
   * Return the video ID.
   *
   * @return string
   *   The video ID.
   */
  public function getId(): string {
    return $this->videoId;
  }

}
