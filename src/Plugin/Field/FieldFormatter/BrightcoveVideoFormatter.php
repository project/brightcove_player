<?php

declare(strict_types=1);

namespace Drupal\brightcove_player\Plugin\Field\FieldFormatter;

use Drupal\brightcove_player\Video;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'brightcove_video_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "brightcove_video_formatter",
 *   label = @Translation("Brightcove Video Player"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   }
 * )
 */
class BrightcoveVideoFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() === 'media';
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      if ($item->value) {
        // String fields.
        $video = Video::fromStudioUrl($item->value);
      }
      else {
        // Link fields.
        $video = Video::fromStudioUrl($item->getValue()['uri']);
      }
      $elements[$delta] = [
        // By setting '#children' instead of '#markup' we avoid Drupal
        // filtering out tags and attributes from our markup.
        '#theme' => 'brightcove_player_iframe',
        '#video_src' => $video->getEmbedSrc(),
      ];
    }

    return $elements;
  }

}
